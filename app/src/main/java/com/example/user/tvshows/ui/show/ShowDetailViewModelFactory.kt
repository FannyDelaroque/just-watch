package com.example.user.tvshows.ui.show

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ShowDetailViewModelFactory (private  val  showId: Int): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ShowDetailViewModel(showId) as T
    }
}