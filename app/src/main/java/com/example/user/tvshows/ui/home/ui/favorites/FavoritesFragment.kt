package com.example.user.tvshows.ui.home.ui.favorites

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.user.tvshows.R
import com.example.user.tvshows.entity.Shows
import com.example.user.tvshows.ui.movies.MoviesActivity
import com.example.user.tvshows.ui.show.ShowActivity
import com.example.user.tvshows.ui.tvshows.TVShowsActivity
import kotlinx.android.synthetic.main.fragment_favorites.view.*

/**
 * fragment affichant la liste de favoris
 */

class FavoritesFragment : Fragment(), FavoriteAdapter.ShowAdapterListener {

    private lateinit var favoriteList: MutableList<Shows>
    private lateinit var favoriteAdapter: FavoriteAdapter
    private var recyclerView: RecyclerView ?= null
   private val favoritesViewModel : FavoritesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_favorites, container, false)
        favoriteList = mutableListOf()
        favoriteAdapter = FavoriteAdapter(this.requireContext(),favoriteList,this)

        recyclerView = view.recyclerViewFavorites
        recyclerView?.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = favoriteAdapter
        }

        //abonnement à la liste de favoris
        favoritesViewModel.favorite.observe(viewLifecycleOwner, Observer { favorites-> updateFavorites(favorites)})

       //favoritesViewModel.favoriteToDelete.observe(viewLifecycleOwner, Observer { show -> onFavoriteSelected(show)})

        return view

    }

    private fun updateFavorites(favorites: List<Shows>) {
        favoriteList.clear()
        favoriteList.addAll(favorites)
        favoriteAdapter.notifyDataSetChanged()
    }

    override fun onFavoriteSelected(show: Shows) {
        favoritesViewModel.deleteToFavorite(show)
        Toast.makeText(this.requireContext(), "${show.name} a été retiré de vos favoris", Toast.LENGTH_SHORT).show()
        }

    override fun onClick(show: Shows) {
        if(show.mediaType == "Série"){
            val intent = Intent(this.context, TVShowsActivity::class.java)
            intent.putExtra(ShowActivity.ID, show.id)
            intent.putExtra(ShowActivity.NAME, show.name)
            startActivity(intent)
        } else{
            val intent = Intent(this.context, MoviesActivity::class.java)
            intent.putExtra(ShowActivity.ID, show.id)
            intent.putExtra(ShowActivity.NAME, show.name)
            startActivity(intent)
        }

    }


}
