package com.example.user.tvshows.ui.home.ui.favorites

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.user.tvshows.App
import com.example.user.tvshows.entity.Shows
import java.util.concurrent.Executors

class FavoritesViewModel : ViewModel() {

    private val _favorite = MutableLiveData<List<Shows>>()
    val favoriteToDelete = MutableLiveData<Shows>()


    val favorite: LiveData<List<Shows>> = App.database.FavoritesDAO().getAllFavorites()



   /* fun displayFavorite() {
        val favoriteDao = App.database.FavoritesDAO()
        Executors.newSingleThreadExecutor().execute {
              favoriteDao.getAllFavorites()
          }
    }*/

    fun deleteToFavorite(show: Shows) {
        Executors.newSingleThreadExecutor().execute {
            App.database.FavoritesDAO().deleteFavorite(show)
        }
    }
}