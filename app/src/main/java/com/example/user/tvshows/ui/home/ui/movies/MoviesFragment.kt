package com.example.user.tvshows.ui.home.ui.movies

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.user.tvshows.entity.Shows
import com.example.user.tvshows.ui.home.ui.show.ShowFragment
import com.example.user.tvshows.ui.movies.MoviesActivity
import com.example.user.tvshows.ui.show.ShowActivity
import com.example.user.tvshows.ui.tvshows.TVShowsActivity


class MoviesFragment : ShowFragment() {



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //chargement de la liste de films
        viewModel.loadTopMovies()

        //abonnement au changement dans la liste de film
        viewModel.show.observe(viewLifecycleOwner, Observer { show -> updateShow(show) })

    }


    override fun onShowSelected(show: Shows) {
        val intent = Intent(this.context, MoviesActivity::class.java)
        intent.putExtra(ShowActivity.ID, show.id)
        intent.putExtra(ShowActivity.NAME, show.name)
        startActivity(intent)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        viewModel.loadMovieFromQuery(query!!)
        searchView.clearFocus()
        return true
    }

    override fun onClose(): Boolean {
        viewModel.loadTopMovies()
        searchView.onActionViewCollapsed()
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        viewModel.loadTopMovies()
        return true
    }




}
