package com.example.user.tvshows.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.user.tvshows.LocalPreferences
import com.example.user.tvshows.R
import com.example.user.tvshows.entity.State
import com.example.user.tvshows.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), LoginResultCallbacks {


    private val loginViewModel : LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

     /*   val activityMainBinding = DataBindingUtil.setContentView<ActivityLoginBinding>(this,R.layout.activity_login)
        activityMainBinding.viewModel = ViewModelProviders.of(this,LoginViewModelFactory(this))
            .get(LoginViewModel::class.java)*/

        // mettre l'editText en inputText = textPassword avec la bonne police
        input_password.transformationMethod = PasswordTransformationMethod()



        // abonnement aux changements d'état de state
        loginViewModel.getState().observe(this, Observer { state -> updateUI(state) })

    }


    private fun updateUI(state: State) {
        when (state){
             State.IS_VALID -> onSuccess()
             State.NOT_VALID -> onError()
            State.EN_COURS -> onPrepare()



        }
    }

    fun onLoginButtonClicked(button: View){
        onPrepare()
        val username = input_username.text.toString()
        val password = input_password.text.toString()
        LocalPreferences(this).username = username
        loginViewModel.userLogin(LocalPreferences(this).access_token,username,password)


    }


    override fun onPrepare() {
        Toast.makeText(this,getString(R.string.checking),Toast.LENGTH_SHORT).show()
    }

    override fun onSuccess() {
        Toast.makeText(this,getString(R.string.login_success),Toast.LENGTH_SHORT).show()
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }

    override fun onError() {
        Toast.makeText(this,getString(R.string.login_error),Toast.LENGTH_SHORT).show()
    }


}




