package com.example.user.tvshows.ui.login


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.user.tvshows.App
import com.example.user.tvshows.entity.State


class LoginViewModel():ViewModel(), LoginResultCallbacks {

    val callbacks: LoginResultCallbacks = this
   /* private val user = MutableLiveData<User>().apply {
        value = User(username = "", password = "")
    }
    val text: LiveData<User> = user*/



    private val state = MutableLiveData<State>().apply {

    }
    fun getState() : LiveData<State> = state



    fun userLogin(token: String, password: String, username: String) {
            App.dataprovider.userLogin(token,password,username,callbacks)



    }

    override fun onPrepare() {
        state.value = State.EN_COURS
    }

    override fun onSuccess() {
        state.value = State.IS_VALID
    }

    override fun onError() {
        state.value = State.NOT_VALID
    }


}