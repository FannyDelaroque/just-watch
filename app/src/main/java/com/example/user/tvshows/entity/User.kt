package com.example.user.tvshows.entity

import android.text.TextUtils
import androidx.databinding.BaseObservable

enum class State(){
    IS_VALID,
    NOT_VALID,
    EN_COURS
}
class User (private var username : String, private var password : String): BaseObservable(){

    val isDataValid : Boolean
    get() = (!TextUtils.isEmpty(getUsername()))

    fun getUsername(): String{
        return username
    }

    fun getPassword(): String{
        return password
    }

    fun setUsername(username: String){
        this.username = username
    }

    fun setPassword(password: String){
        this.password = password
    }
}