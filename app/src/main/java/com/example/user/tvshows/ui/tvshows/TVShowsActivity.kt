package com.example.user.tvshows.ui.tvshows

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.user.tvshows.ui.show.ShowActivity

class TVShowsActivity : ShowActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(showId != null) {
            viewModel.loadShow(showId)
        }

        // abonnement pour afficher le show sélectionné par showId
        viewModel.show.observe(this, Observer { show -> displayShow(show) })



        // abonnement pour voir si le show affiché est déjà enregistré dans les favoris
        viewModel.currentShow.observe(this, Observer { show -> update(show) })

    }
}
