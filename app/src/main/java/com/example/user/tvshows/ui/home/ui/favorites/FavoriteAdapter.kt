package com.example.user.tvshows.ui.home.ui.favorites

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.user.tvshows.R
import com.example.user.tvshows.entity.Shows
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_favorites.view.*

class FavoriteAdapter (var context: Context, var favoriteList: List<Shows>, val listener: ShowAdapterListener ):RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder>() {

    interface ShowAdapterListener{
        fun onFavoriteSelected(show:Shows)
        fun onClick(show: Shows)
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
      val rootView = LayoutInflater.from(parent.context).inflate(R.layout.detail_favorites,parent,false)
        val holder = FavoriteViewHolder(rootView)
        return holder
    }

    override fun getItemCount(): Int {
        return favoriteList.size
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val favorite = favoriteList[position]
        if (favorite != null) {
            holder.fillWithShow(favorite)
        }

    }
       fun onFavoriteClick(position: Int, context: Context?) {
           listener.onFavoriteSelected(favoriteList[position])

        }

        fun onSelected(position: Int,context: Context){
            listener.onClick(favoriteList[position])
        }



    inner class FavoriteViewHolder (rootView: View):RecyclerView.ViewHolder(rootView), View.OnClickListener{
        private val image = rootView.imageViewFavorites
        private val title = rootView.textViewTitleFavorite
        private val mediaType = rootView.textViewMediaTypeFavorite
        private val date = rootView.textViewMediaDateFavorite
        private val button = rootView.imageViewHeart
        private val container = rootView.containerFavorite

        init {
            button.setOnClickListener(this)
            container.setOnClickListener(this)
        }

        fun fillWithShow(show: Shows){
            title.text = show.name
            Picasso.with(context).load(show.posterpath).into(image)
            mediaType.text = show.mediaType
            date.text = show.date


        }

        override fun onClick(v: View) {
            if (v.id != null){
                when(v.id){
                 R.id.imageViewHeart -> onFavoriteClick(adapterPosition, v.context)
                 R.id.containerFavorite ->onSelected(adapterPosition,v.context)
                }

            }

        }



    }


}