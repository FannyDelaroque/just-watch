package com.example.user.tvshows.ui.home.ui.show

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.GridView
import android.widget.SearchView
import androidx.fragment.app.viewModels

import com.example.user.tvshows.R
import com.example.user.tvshows.entity.Shows
import kotlinx.android.synthetic.main.show_fragment.view.*

/**
 * classe modèle pour les fragment TVShowsFragment et MovieFragment qui affichent une liste de séries ou films
 */

open class ShowFragment : Fragment(), ShowAdapter.ShowAdapterListener,
    androidx.appcompat.widget.SearchView.OnQueryTextListener,
     MenuItem.OnActionExpandListener, androidx.appcompat.widget.SearchView.OnCloseListener {

    var gridView : GridView?= null
    val viewModel: ShowViewModel by viewModels()
    lateinit var showAdapter : ShowAdapter
    lateinit var showList: MutableList<Shows>
    lateinit var searchView: androidx.appcompat.widget.SearchView
    lateinit var searchItem: MenuItem

    companion object {
        fun newInstance() =
            ShowFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view= inflater.inflate(R.layout.show_fragment, container, false)
        showList = mutableListOf()
/*
        //chargement des shows
        viewModel.loadTopShows()

        // abonnement aux changements de show
        viewModel.show.observe(viewLifecycleOwner, Observer { show -> updateShow(show) })*/



        return  view
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    // ajout des shows à la list de TVShow pour les insérer ensuite dans l'adapter
    fun updateShow(show: List<Shows>?) {
        if (show != null) {
            showList.clear()
            showList.addAll(show)
            showAdapter = ShowAdapter(
                showList,
                requireContext(),
                this
            )
            gridView = view?.gridviewShows
            gridView?.adapter = showAdapter


        }
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
       // menu.clear()
        super.onCreateOptionsMenu(menu, menuInflater)
        menuInflater.inflate(R.menu.home, menu)
        searchItem = menu?.findItem(R.id.action_settings)
        searchView = searchItem?.actionView as androidx.appcompat.widget.SearchView
        searchView.setOnQueryTextListener(this)
        searchView.setOnCloseListener(this)
        searchItem.setOnActionExpandListener(this)


    }


    override fun onShowSelected(show: Shows) {

    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
       return true
    }

    override fun onClose(): Boolean {
        return true
    }

    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        return true
    }


}
