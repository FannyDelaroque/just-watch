package com.example.user.tvshows

import android.app.Application
import androidx.room.Room
import com.example.user.tvshows.database.AppDatabase
import com.example.user.tvshows.database.DATABASE_NAME
import com.example.user.tvshows.repository.Api
import timber.log.Timber
import java.util.concurrent.Executors


class App: Application() {

    companion object{
        lateinit var database: AppDatabase
        lateinit var dataprovider : Api
    }

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        database = Room.databaseBuilder(this, AppDatabase::class.java, DATABASE_NAME)
            .build()
        Executors.newSingleThreadExecutor().execute {
            database.FavoritesDAO().getAllFavorites()

        }
        dataprovider = Api(this)
        dataprovider.getToken{token, error ->
            if (token != null) {
                LocalPreferences(this).access_token = token
            }
        }




    }
}