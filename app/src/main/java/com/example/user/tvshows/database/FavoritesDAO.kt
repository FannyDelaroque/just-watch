package com.example.user.tvshows.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.user.tvshows.entity.Shows

@Dao
interface FavoritesDAO {

    /**
     * Usage:
     * dao.insertFavorite(show)
     */

    @Insert
    fun insertFavorite(show: Shows)

    /**
     * Usage:
     * dao.deleteFavorite(favorite)
     */
    @Delete
    fun deleteFavorite(show: Shows)

    /**
     * dao.getAllFavorites().observe(this, Observer{ favorite -> ...}
     * avec le liveData on s'abonne au changement
     */
    @Query("SELECT * FROM shows")
        fun getAllFavorites(): LiveData<List<Shows>>

    /**
     * dao.getFavoriteById(3).observe(this, Observer{ favorite -> ...}
     */
    @Query("SELECT *FROM shows WHERE id= :showId")
    fun getFavoritesById(showId: Int): LiveData<Shows>





}