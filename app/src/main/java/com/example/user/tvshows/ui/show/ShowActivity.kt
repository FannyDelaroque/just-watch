package com.example.user.tvshows.ui.show

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.user.tvshows.R
import com.example.user.tvshows.entity.Shows
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_shows.*
import timber.log.Timber

/**
 * classe modèle pour TVShowActivity et MoviesActivity qui affichent le détail d'un film ou d'une série
 */

open class ShowActivity: AppCompatActivity() {

    lateinit var viewModel : ShowDetailViewModel
    private lateinit var showName :String
     var showId : Int = 0
    private lateinit var favoriteShow : Shows
    private var isClicked: Boolean ?= null


    companion object{
        const val ID = "id"
        const val NAME = "name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shows)


        showId = intent.getIntExtra(ID, 0)
        showName= intent.getStringExtra(NAME)

        Timber.d("id selected =  $showId")

        val factory = ShowDetailViewModelFactory(showId)
        viewModel = ViewModelProvider(this, factory).get(ShowDetailViewModel::class.java)

    }

    fun update(show: Shows?){
        if (show != null){
            addFavoriteButton.setImageResource(R.drawable.ic_favorite_black_24dp)
            addFavoriteButton.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorPrimary)
            isClicked = true
        }
    }


    fun displayShow(show: Shows?) {

        if(show != null){
            textViewCountry.text =show.originCountry
            textViewDate.text = show.date
            textViewMediaType.text = show.mediaType
            textViewTitle.text= show.name
            textViewOverview.text = show.overview
            Picasso.with(this).load(show.backdropPath).into(imageViewDetail)



        }
    }

    private fun deleteFromFavorite(){
        viewModel.deleteToFavorite()
        Toast.makeText(this, "$showName a bien été retiré de vos favoris", Toast.LENGTH_SHORT).show()
        addFavoriteButton.setImageResource(R.drawable.ic_favorite_border_black_24dp)
        addFavoriteButton.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorPrimary)
        isClicked = false
    }

    private fun addToFavorite(){
        viewModel.addToFavorite()
        Toast.makeText(this, "$showName a bien été ajouté à vos favoris", Toast.LENGTH_SHORT).show()
        addFavoriteButton.setImageResource(R.drawable.ic_favorite_black_24dp)
        addFavoriteButton.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorPrimary)
        isClicked = true

    }

    fun manageFavorite(view: View){
        if(isClicked == true){
            deleteFromFavorite()
        } else{
            addToFavorite()
        }

    }
}