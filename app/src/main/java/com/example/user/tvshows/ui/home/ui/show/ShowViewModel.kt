package com.example.user.tvshows.ui.home.ui.show

import androidx.lifecycle.*
import com.example.user.tvshows.App
import com.example.user.tvshows.entity.Shows

class ShowViewModel : ViewModel() {
    private val _shows = MutableLiveData<List<Shows>>()

    val show: LiveData<List<Shows>> = _shows


    fun loadTopShows(){
        App.dataprovider.loadTopTVShows { tvShowList, error ->
            _shows.value = tvShowList
        }
    }

    fun loadTopMovies(){
        App.dataprovider.loadTopMovies { showList, error ->
            _shows.value = showList
        }
    }

    fun loadTVShowFromQuery(query: String){
        App.dataprovider.getTVShowsForQuery(query){
            showList, error ->  _shows.value =showList
        }
    }

    fun loadMovieFromQuery(query: String){
        App.dataprovider.getMovieForQuery(query){
            movieList, error-> _shows.value = movieList
        }
    }

}
