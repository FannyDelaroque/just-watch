package com.example.user.tvshows

import android.content.Context
import android.preference.PreferenceManager

class LocalPreferences (context: Context) {
    val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    var access_token : String
    get() {
        return sharedPreferences.getString("access_token", null)
    }
    set(value) {
        sharedPreferences.edit().putString("access_token",value).apply()
    }
    var username : String
        get() {
            return sharedPreferences.getString("username", null)
        }
        set(value) {
            sharedPreferences.edit().putString("username",value).apply()
        }

}