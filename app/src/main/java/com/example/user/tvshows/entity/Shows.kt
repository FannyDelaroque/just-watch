package com.example.user.tvshows.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

// Serializable permettra de passer un objet de type TVShow dans un intent
@Entity (tableName = "shows")
data class Shows (
    @PrimaryKey(autoGenerate = false)
                    var id: Int,
                    val name: String,
                    val overview : String,
                    val posterpath: String,
                    val backdropPath : String,
                    val vote: Int,
                    val date: String?,
                    val originCountry: String,
                    val mediaType: String
): Serializable{




}