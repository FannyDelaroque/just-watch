package com.example.user.tvshows.repository

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.user.tvshows.entity.Shows
import com.example.user.tvshows.ui.login.LoginResultCallbacks
import org.json.JSONObject
import timber.log.Timber
import java.net.URLEncoder
import java.util.*

private const val POSTER_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w780/"
private const val BACKDROP_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/"
private const val API_BASE_URL= "https://api.themoviedb.org/3/"
private const val API_KEY = "f6ca13cd71f090bdd508521c36bf0b63"
private const val LANGUAGE = "fr-FR"



enum class ApiUrls(val path:String){
    TvShowPopular("trending/tv/week"),
    MoviePopular("trending/movie/week"),
    TVShow("tv/"),
    Movie("movie/"),
    SearchTV("search/tv"),
    SearchMovie("search/movie"),
    Token("authentication/token/new"),
    Authentication("authentication/token/validate_with_login");


    private val queryParameters = mutableMapOf<String,String>()
    private var value : Int ?= null
    fun buildUrl():String{
        addUrlParameters("api_key",API_KEY)
        addUrlParameters("language", LANGUAGE)

        val builder = StringBuilder(API_BASE_URL)
        builder.append(path)
        value?.let { builder.append(it) }

        for (entry in queryParameters){
            if (builder.contains("?")){
                builder.append("&")
            }else{
                builder.append("?")
            }
            builder.append(entry.key + "=" + URLEncoder.encode(entry.value,"UTF-8"))
        }





        return builder.toString()
    }

    fun addUrlParameters(key: String, value:String):ApiUrls{
        queryParameters[key]=value
        return this
    }

    fun addVariableParameters(value: Int): ApiUrls{
        this.value = value
        return this
    }


}


class Api (context: Context){

    private val requestQueue:RequestQueue = Volley.newRequestQueue(context)


    /**
     * Fonction get permettant de récupérer un token lors de l'ouvertur de l'application
     */
    fun getToken(resultHandler: (token: String?, error: String?) -> Unit){
        getTokenFromUrl(ApiUrls.Token.buildUrl(),resultHandler)
    }

    /**
     * Fonction POST permattant de se connecter.
     * Prend en compte 3 paramètres: token(reçu via getToken(), username, password
     */
    fun userLogin(access_token: String, username:String, password: String, callbacks: LoginResultCallbacks){
        val jsonobject = JSONObject()
        jsonobject.put("username", username)
        jsonobject.put("password", password)
        jsonobject.put("request_token", access_token)
        val request = object :JsonObjectRequest(
            Method.POST,
            ApiUrls.Authentication.buildUrl(),
            jsonobject,
            Response.Listener<JSONObject> {
                Timber.d("ok")
                callbacks.onSuccess()
            },
            Response.ErrorListener {
                Timber.d("not ok")
                callbacks.onError()

            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["credentials"] = "username + password"
                headers["Content-Type"] = "application/json;charset=utf-8"
                headers["Authorization"] = "Bearer $access_token"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                val params = HashMap<String, String>()
                params["username"] = username
                params["password"] = password
                return params
            }
        }
        requestQueue.add(request)
    }

    /**
     * Fonction GET permettant de charger le détail d'une série via un ID transmis en paramètre
     */
    fun loadShow(variable: Int,resultHandler: (show: Shows?, error: String?) -> Unit){
    getShowFromUrl(ApiUrls.TVShow.addVariableParameters(variable).buildUrl(),resultHandler)
    }

    /**
     * Fonction GET permettant de charger le détail d'un film via un ID transmis en paramètre
     */
    fun loadMovie(variable: Int,resultHandler: (show: Shows?, error: String?) -> Unit){
        getMovieFromUrl(ApiUrls.Movie.addVariableParameters(variable).buildUrl(),resultHandler)
    }

    /**
     * Fontion GET permettant de charger la liste de films les plus tendances de la semaine
     */
    fun loadTopMovies(resultHandler: (showList: List<Shows>, error: String?) -> Unit){
        getMovieListFromUrl(ApiUrls.MoviePopular.buildUrl(), resultHandler)
    }

    /**
     * Fontion GET permettant de charger la liste de séries les plus tendances de la semaine
     */
    fun loadTopTVShows(resultHandler: (showList: List<Shows>, error: String?) -> Unit){
        getTVShowFromUrl(ApiUrls.TvShowPopular.buildUrl(), resultHandler)
    }

    /**
     * fonction get permettant de rechercher une série par un nom
     */
    fun getTVShowsForQuery(query:String, resultHandler: (showList: List<Shows>, error: String?) -> Unit){
        getTVShowFromUrl(ApiUrls.SearchTV.addUrlParameters("query",query).buildUrl(),resultHandler)
    }

    /**
     * fonction get permettant de rechercher une film par un nom
     */
    fun getMovieForQuery(query:String, resultHandler: (showList: List<Shows>, error: String?) -> Unit){
        getMovieListFromUrl(ApiUrls.SearchMovie.addUrlParameters("query",query).buildUrl(),resultHandler)
    }



    private fun getMovieFromUrl(url: String, resultHandler: (show: Shows?, error: String?) -> Unit) {
        val request = JsonObjectRequest(
            url,
            null,
            Response.Listener {
                val show = parseMovieResponse(it)
                //si succes renvoit une liste de TVShow
                resultHandler(show,null)
            },
            Response.ErrorListener {
                // sinon renvoit un message d'erreur
                resultHandler(null,it.localizedMessage)
            }
        )
        requestQueue.add(request)
    }

    private fun parseMovieResponse(jsonobject: JSONObject): Shows {
        val tvShowId = jsonobject.getInt("id")
        var tvShowName = jsonobject.getString("original_title")
        val tvShowDescription = jsonobject.getString("overview")
        val tvShowPosterPath = jsonobject.getString("poster_path")
        val tvShowBackDropPath = jsonobject.getString("backdrop_path")
        val tvShowVoteAverage = jsonobject.getInt("vote_average")
        val tvShowDate = jsonobject.getString("release_date")
        val tvShowOriginCountry = jsonobject.getString("original_language")
        val posterImage = POSTER_IMAGE_BASE_URL+ tvShowPosterPath
        val backdropImage = BACKDROP_IMAGE_BASE_URL + tvShowBackDropPath

        val date = tvShowDate.substring(0,4)
        val show = Shows(tvShowId,tvShowName,tvShowDescription,posterImage,
            backdropImage,tvShowVoteAverage,date,tvShowOriginCountry, "Film")
        return show
    }

    private fun getMovieListFromUrl(url: String, resultHandler: (showList: List<Shows>, error: String?) -> Unit) {
        val request = JsonObjectRequest(
            url,
            null,
            Response.Listener {
                val tvShowList = parseMovieListResponse(it)
                //si succes renvoit une liste de TVShow
                resultHandler(tvShowList,null)
            },
            Response.ErrorListener {
                // sinon renvoit un message d'erreur
                resultHandler(listOf(),it.localizedMessage)
            }
        )
        requestQueue.add(request)
    }

    private fun getTVShowFromUrl(url:String, resultHandler:(showList: List<Shows>, error:String?)->Unit){
        val request = JsonObjectRequest(
            url,
            null,
            Response.Listener {
                val tvShowList = parseShowListResponse(it)
                //si succes renvoit une liste de TVShow
                resultHandler(tvShowList,null)
            },
            Response.ErrorListener {
                // sinon renvoit un message d'erreur
                resultHandler(listOf(),it.localizedMessage)
            }
        )
        requestQueue.add(request)
    }

    private fun getTokenFromUrl(url: String, resultHandler: (token: String?, error: String?) -> Unit) {
        val request = JsonObjectRequest(
            url,
            null,
            Response.Listener {
                val token = parseTokenResponse(it)
                resultHandler(token,null)
            },
            Response.ErrorListener {
                // sinon renvoit un message d'erreur
                resultHandler(null,it.localizedMessage)
            }
        )
        requestQueue.add(request)
    }

    private fun getShowFromUrl(url: String, resultHandler: (show: Shows?, error: String?) -> Unit) {
        val request = JsonObjectRequest(
            url,
            null,
            Response.Listener {
                val show = parseShowResponse(it)
                //si succes renvoit une liste de TVShow
                resultHandler(show,null)
            },
            Response.ErrorListener {
                // sinon renvoit un message d'erreur
                resultHandler(null,it.localizedMessage)
            }
        )
        requestQueue.add(request)
    }

    private fun parseTokenResponse(it: JSONObject): String? {
        val token = it.getString("request_token")
        return token
    }

    private fun parseShowResponse(jsonobject: JSONObject): Shows {
        val tvShowId = jsonobject.getInt("id")
        var tvShowName = jsonobject.getString("original_name")
        val tvShowDescription = jsonobject.getString("overview")
        val tvShowPosterPath = jsonobject.getString("poster_path")
        val tvShowBackDropPath = jsonobject.getString("backdrop_path")
        val tvShowVoteAverage = jsonobject.getInt("vote_average")
        val tvShowDate = jsonobject.getString("first_air_date")
        val tvShowOriginCountry = jsonobject.getString("origin_country")
        val posterImage = POSTER_IMAGE_BASE_URL+ tvShowPosterPath
        val backdropImage = BACKDROP_IMAGE_BASE_URL + tvShowBackDropPath

        val date = tvShowDate.substring(0,4)
       val show = Shows(tvShowId,tvShowName,tvShowDescription,posterImage,
           backdropImage,tvShowVoteAverage,date,tvShowOriginCountry, "Série")
        return show
    }

    private fun parseShowListResponse(jsonResponseBody: JSONObject): List<Shows> {

        val jsonTVShowList = jsonResponseBody.getJSONArray("results")
        val tvShowList = mutableListOf<Shows>()
        if(jsonTVShowList != null){
            for(tvShowIndex in 0 until jsonTVShowList.length()){
                val tvShowJson = jsonTVShowList.getJSONObject(tvShowIndex)
                if (tvShowJson != null){
                    val tvShowId = tvShowJson.getInt("id")
                    val tvShowName = tvShowJson.getString("original_name")
                    val tvShowDescription = tvShowJson.getString("overview")
                    val tvShowPosterPath = tvShowJson.getString("poster_path")
                    val tvShowBackDropPath = tvShowJson.getString("backdrop_path")
                    val tvShowVoteAverage = tvShowJson.getInt("vote_average")
                    var tvShowDate : String ?= null
                    var date: String ?=null
                    if (tvShowJson.has("first_air_date") && tvShowJson.getString("first_air_date") != "") {
                        tvShowDate = tvShowJson.getString("first_air_date")
                        date = tvShowDate.substring(0,4)
                    }

                    else{
                        tvShowDate = ""
                        date = ""
                    }

                    val tvShowOriginCountry = tvShowJson.getString("origin_country")
                    val tvShowMediaType = "Série"
                    val posterImage = POSTER_IMAGE_BASE_URL+ tvShowPosterPath
                    val backdropImage = BACKDROP_IMAGE_BASE_URL + tvShowBackDropPath
                    tvShowList.add(Shows(id=tvShowId, name= tvShowName,overview = tvShowDescription,
                        posterpath = posterImage,backdropPath = backdropImage, vote= tvShowVoteAverage, date = date,
                        originCountry = tvShowOriginCountry, mediaType = tvShowMediaType))
                }
            }
        }
        return tvShowList
    }

    private fun parseMovieListResponse(jsonResponseBody: JSONObject): List<Shows> {

        val jsonTVShowList = jsonResponseBody.getJSONArray("results")
        val movieList = mutableListOf<Shows>()
        if(jsonTVShowList != null){
            for(tvShowIndex in 0 until jsonTVShowList.length()){
                val movieJson = jsonTVShowList.getJSONObject(tvShowIndex)
                if (movieJson != null){
                    val movieId = movieJson.getInt("id")
                    val movieName = movieJson.getString("original_title")
                    val movieDescription = movieJson.getString("overview")
                    val moviePosterPath = movieJson.getString("poster_path")
                    val movieBackDropPath = movieJson.getString("backdrop_path")
                    val movieVoteAverage = movieJson.getInt("vote_average")
                    var movieDate : String ?= null
                    var date: String ?=null
                    if(movieJson.getString("release_date") == ""){
                        movieDate = ""
                        date = ""
                    }
                    else {
                        movieDate = movieJson.getString("release_date")
                        date = movieDate.substring(0,4)
                    }
                    val movieOriginCountry = movieJson.getString("original_language")
                    val movieMediaType = "Film"
                    val posterImage = POSTER_IMAGE_BASE_URL+ moviePosterPath
                    val backdropImage = BACKDROP_IMAGE_BASE_URL + movieBackDropPath
                    movieList.add(Shows(id=movieId, name= movieName,overview = movieDescription,
                        posterpath = posterImage,backdropPath = backdropImage, vote= movieVoteAverage, date = date,
                        originCountry = movieOriginCountry, mediaType = movieMediaType))
                }
            }
        }
        return movieList
    }


}