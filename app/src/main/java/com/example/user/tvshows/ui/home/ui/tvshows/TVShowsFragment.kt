package com.example.user.tvshows.ui.home.ui.tvshows

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import com.example.user.tvshows.entity.Shows
import com.example.user.tvshows.ui.home.ui.show.ShowFragment
import com.example.user.tvshows.ui.show.ShowActivity
import com.example.user.tvshows.ui.tvshows.TVShowsActivity


class TVShowsFragment : ShowFragment(){



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //chargement des shows
        viewModel.loadTopShows()

        // abonnement aux changements de show
        viewModel.show.observe(viewLifecycleOwner, Observer { show -> updateShow(show) })

    }

    override fun onShowSelected(show: Shows) {
        val intent = Intent(this.context, TVShowsActivity::class.java)
        intent.putExtra(ShowActivity.ID, show.id)
        intent.putExtra(ShowActivity.NAME, show.name)
        startActivity(intent)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        viewModel.loadTVShowFromQuery(query!!)
        searchView.clearFocus()
            return true
    }

    override fun onClose(): Boolean {
        viewModel.loadTopShows()
        searchView.onActionViewCollapsed()
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        viewModel.loadTopShows()
        return true
    }



}
