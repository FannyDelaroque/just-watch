package com.example.user.tvshows.ui.login


interface LoginResultCallbacks{
    fun onPrepare()
    fun onSuccess()
    fun onError()

}