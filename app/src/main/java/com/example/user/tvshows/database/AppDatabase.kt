package com.example.user.tvshows.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.user.tvshows.entity.Shows

const val DATABASE_NAME = "favorites"

@Database(entities = [Shows::class], version = 1)
// abstract car Room génère le contenu
abstract class AppDatabase: RoomDatabase() {
    abstract fun FavoritesDAO() : FavoritesDAO
}

