package com.example.user.tvshows.ui.home.ui.show

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.user.tvshows.R
import com.example.user.tvshows.entity.Shows
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.show_or_movie.view.*


class ShowAdapter(var showList: List<Shows>, var context: Context, val listener: ShowAdapterListener): BaseAdapter(){

interface ShowAdapterListener{
    fun onShowSelected(show: Shows)
}

    fun reloadList(list: List<Shows>){
        showList = list
        notifyDataSetChanged()
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val rootView = View.inflate(context,R.layout.show_or_movie, null)
        val container = rootView.container
        val title : TextView = rootView.title
        val pathPoster: ImageView = rootView.pathPoster
        val note: TextView = rootView.note
        val year: TextView = rootView.year
        val originCountry: TextView = rootView.originCountry

        title.text = showList[position].name
        note.text = showList[position].vote.toString()
        year.text = showList[position].date
        originCountry.text = showList[position].originCountry
        Picasso.with(context).load(showList[position].posterpath).into(pathPoster)

        // permet d'afficher le détail lorsqu'on clique sur un élément du conteneur
        container.setOnClickListener { listener.onShowSelected(showList[position]) }


        return rootView
    }


    override fun getItem(position: Int): Any {
        return showList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
       return showList.size
    }




}

