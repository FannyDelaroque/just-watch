package com.example.user.tvshows.ui.show

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.user.tvshows.App
import com.example.user.tvshows.entity.Shows
import java.util.concurrent.Executors

class ShowDetailViewModel(id: Int): ViewModel() {

    private val showLiveData = MutableLiveData<Shows>()
    val show: LiveData<Shows> = showLiveData


    private lateinit var  favoriteShow : Shows


    private val showIdLiveData = MutableLiveData<Int>()
    val currentShow : LiveData<Shows> = Transformations.switchMap(showIdLiveData){
        id -> App.database.FavoritesDAO().getFavoritesById(id)
    }

    init {
        showIdLiveData.value = id
    }

    fun loadMovie(index: Int){
        App.dataprovider.loadMovie(index){
                show,error->
            showLiveData.value = show
            if (show != null) {
                favoriteShow = show
            }
        }
    }

    fun loadShow(index: Int){
       App.dataprovider.loadShow(index){
           show,error->
           showLiveData.value = show
           if (show != null) {
               favoriteShow = show
           }
       }
   }

    fun addToFavorite(){
        Executors.newSingleThreadExecutor().execute {
            App.database.FavoritesDAO().insertFavorite(favoriteShow)
        }

    }

    fun deleteToFavorite(){
        Executors.newSingleThreadExecutor().execute {
            App.database.FavoritesDAO().deleteFavorite(favoriteShow)
        }
    }




}