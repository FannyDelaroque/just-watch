package com.example.user.tvshows

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.user.tvshows.ui.login.LoginViewModel
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    @Test
    fun isLoginCorrect() {
        val viewModel = LoginViewModel()
        val userName = "roger"
        val userPassword = "123567"
        val liveData = viewModel.login(userName,userPassword)
        assertEquals(liveData,viewModel.login(username= userName, password = userPassword))
                }
}
